﻿using System;
using System.Collections.Generic;

namespace BeeKid.API.Models
{
    public partial class TbSection2
    {
        public int Section2Id { get; set; }
        public string Section2Title { get; set; }
        public string Section2Description { get; set; }
        public string Section2ThinkingTitle { get; set; }
        public string Section2BrainTitle { get; set; }
        public string Section2CreationTitle { get; set; }
        public string Section2MotorTitle { get; set; }
        public string Section2Image { get; set; }
        public string Section2BrainContent { get; set; }
        public string Section2CreationContent { get; set; }
        public string Section2MotorContent { get; set; }
        public string Section2ThinkingContent { get; set; }
    }
}
