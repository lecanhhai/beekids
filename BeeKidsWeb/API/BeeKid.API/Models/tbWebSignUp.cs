﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeeKid.API.Models
{
    public class tbWebSignUp
    {
        public int webSignupId { get; set; }
        public string webSignupName { get; set; }
        public string webSignupEmail { get; set; }
        public string webSignupPhonenb { get; set; }
        public string webSignupCenterName { get; set; }
    }
}
