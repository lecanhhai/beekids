﻿using System;
using System.Collections.Generic;

namespace BeeKid.API.Models
{
    public partial class TbSection1
    {
        public int Section1Id { get; set; }
        public string Section1Title { get; set; }
        public string Section1Description { get; set; }
        public string Section1ProductTitle { get; set; }
        public string Section1PriceTitle { get; set; }
        public string Section1DeliveryTitle { get; set; }
        public string Section1QualityTitle { get; set; }
        public string Section1ProductContent { get; set; }
        public string Section1DeliveryContent { get; set; }
        public string Section1QualityContent { get; set; }
        public string Section1PriceContent { get; set; }
    }
}
