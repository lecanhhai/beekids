﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BeeKid.API.Models;
using Microsoft.AspNetCore.Cors;

namespace BeeKid.API.Controllers
{
    [EnableCors("AllowMyOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class tbWebSignUpsController : ControllerBase
    {
        private readonly DB_Context _context;

        public tbWebSignUpsController(DB_Context context)
        {
            _context = context;
        }

        // GET: api/tbWebSignUps
        [HttpGet]
        public async Task<ActionResult<IEnumerable<tbWebSignUp>>> GettbWebSignUp()
        {
            return await _context.tbWebSignUp.ToListAsync();
        }

        // GET: api/tbWebSignUps/5
        [HttpGet("{id}")]
        public async Task<ActionResult<tbWebSignUp>> GettbWebSignUp(int id)
        {
            var tbWebSignUp = await _context.tbWebSignUp.FindAsync(id);

            if (tbWebSignUp == null)
            {
                return NotFound();
            }

            return tbWebSignUp;
        }

        //GET: Search
        [HttpGet("search")]
        public async Task<ActionResult<IEnumerable<tbWebSignUp>>> Searchtitle([FromQuery] string keys)
        {


            IQueryable<tbWebSignUp> signup = _context.tbWebSignUp;
            if (!string.IsNullOrEmpty(keys))
            {
                signup = signup.Where(x => x.webSignupName.Contains(keys));
            }
            return await signup.ToListAsync();
        }

        // PUT: api/tbWebSignUps/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PuttbWebSignUp(int id, tbWebSignUp tbWebSignUp)
        {
            if (id != tbWebSignUp.webSignupId)
            {
                return BadRequest();
            }

            _context.Entry(tbWebSignUp).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tbWebSignUpExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/tbWebSignUps
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<tbWebSignUp>> PosttbWebSignUp(tbWebSignUp tbWebSignUp)
        {
            _context.tbWebSignUp.Add(tbWebSignUp);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GettbWebSignUp", new { id = tbWebSignUp.webSignupId }, tbWebSignUp);
        }

        // DELETE: api/tbWebSignUps/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<tbWebSignUp>> DeletetbWebSignUp(int id)
        {
            var tbWebSignUp = await _context.tbWebSignUp.FindAsync(id);
            if (tbWebSignUp == null)
            {
                return NotFound();
            }

            _context.tbWebSignUp.Remove(tbWebSignUp);
            await _context.SaveChangesAsync();

            return tbWebSignUp;
        }

        private bool tbWebSignUpExists(int id)
        {
            return _context.tbWebSignUp.Any(e => e.webSignupId == id);
        }
    }
}
