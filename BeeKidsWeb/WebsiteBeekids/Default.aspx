﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteMasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>




<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="higlobal" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hislider" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibelowtop" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="slide-main1" style="">
        <div id="rpslide" class="slide-block1 owl-carousel owl-theme">
            <%--<div class="item bg-item">
                <div class="container">
                    <div class=" justify-content-center">
                        <div class="content-block wow fadeInLeft text-left" data-wow-delay="1.5s">
                            <div class="small-title th-bg badge">education makes bright your future</div>
                            <h2>BEEKIDS
                                <br />
                                Câu Chuyện Đồ Gỗ </h2>
                            <p>
                                Đến với Beekids, các bé sẽ phát triển trí não toàn diện trong môi trường giáo dục chất lượng cao.
                            </p>
                            <a class="btn btn-slide" href="#">Liên hệ ngay
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item bg-item">
                <div class="container">
                    <div class=" justify-content-center">
                        <div class="content-block wow fadeInLeft text-left" data-wow-delay="1.5s">
                            <div class="small-title th-bg badge">education makes bright your future</div>
                            <h2>BEEKIDS
                                <br />
                                Câu Chuyện Đồ Gỗ </h2>
                            <p>
                                Đến với Beekids, các bé sẽ phát triển trí não toàn diện trong môi trường giáo dục chất lượng cao.
                            </p>
                            <a class="btn btn-slide" href="#">Liên hệ ngay
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item bg-item">
                <div class="container">
                    <div class="justify-content-center">
                        <div class=" content-block wow fadeinleft text-center" data-wow-delay="1.5s">
                            <div class="small-title th-bg badge">education makes bright your future</div>
                            <h2>BEEKIDS
                                <br />
                                câu chuyện đồ gỗ </h2>
                            <p>
                                Đến với Beekids, các bé sẽ phát triển trí não toàn diện trong môi trường giáo dục chất lượng cao.
                            </p>
                            <a class="btn btn-slide" href="#">Liên hệ ngay
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item bg-item">
                <div class="container">
                    <div class="justify-content-center">
                        <div class=" content-block wow fadeinleft text-right" data-wow-delay="1.5s">
                            <div class="small-title th-bg badge">education makes bright your future</div>
                            <h2>BEEKIDS
                                <br />
                                câu chuyện đồ gỗ </h2>
                            <p>
                                Đến với Beekids, các bé sẽ phát triển trí não toàn diện trong môi trường giáo dục chất lượng cao.
                            </p>
                            <a class="btn btn-slide" href="#">Liên hệ ngay
                            </a>
                        </div>
                    </div>
                </div>
            </div>--%>
        </div>
    </div>
    <section id="nentang" class="pt-130 pb-100" style="position: relative;">
        <!--<div class="fake-title">
            <h6 class="title">about</h6>
        </div>-->
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 align-self-lg-center">
                    <div id="caption" class="about-caption">
                        <h6 id="txtTitleSection1" class="title"></h6>
                        <p id="txtDescriptionSection1"></p>
                        <a class="btn btn-bigsize" href="#">Xem thêm</a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <!-- Services Start -->
                            <div class="services-7">
                                <span class="icon-block th-cl">
                                    <img src="/images/ico-3.png" alt=""></span>
                                <h4 id="txtProductTitle" class="title"></h4>
                                <p id="txtProductContent">
                                </p>
                            </div>
                            <!-- Services End -->
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <!-- Services Start -->
                            <div class="services-7">
                                <span class="icon-block th-cl">
                                    <img src="/images/ico-2.png" alt=""></span>

                                <h4 id="txtPriceTitle" class="title"></h4>
                                <p id="txtPriceContent">
                                </p>
                            </div>
                            <!-- Services End -->
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <!-- Services Start -->
                            <div class="services-7">
                                <span class="icon-block th-cl">
                                    <img src="/images/ico-4.png" alt=""></span>

                                <h4 id="txtDeliveryTitle" class="title"></h4>
                                <p id="txtDeliveryContent">
                                </p>
                            </div>
                            <!-- Services End -->
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <!-- Services Start -->
                            <div class="services-7">
                                <span class="icon-block th-cl">
                                    <img src="/images/ico-1.png" alt=""></span>

                                <h4 id="txtQualityTitle" class="title"></h4>
                                <p id="txtQualityContent">
                                </p>
                            </div>
                            <!-- Services End -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pt-121 pb-82 gray-bg aboutus-bg-3">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12 ">
                    <div class="about-caption">
                        <h6 id="txtTitleSection2" class="title"></h6>
                        <p id="txtDescriptionSection2"></p>
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <!-- Services Start -->
                                <div class="services-8 d-flex align-items-center">
                                    <!--<span class="icon-block pearpal-bg"><img src="/images/cancel.png" alt=""></span>-->
                                    <span class="icon-block pearpal-bg"><i class="icofont-brainstorming"></i></span>
                                    <div class="overflow-text">
                                        <h4 id="txtThinkingTitle" class="title"></h4>
                                        <p id="txtThinkingContent"></p>
                                    </div>
                                </div>
                                <!-- Services End -->
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <!-- Services Start -->
                                <div class="services-8 d-flex align-items-center">
                                    <!--<span class="icon-block th-bg"><img src="/images/podcast.png" alt=""></span>-->
                                    <span class="icon-block th-bg"><i class="icofont-brainstorming"></i></span>
                                    <div class="overflow-text">
                                        <h4 id="txtBrainTitle" class="title"></h4>
                                        <p id="txtBrainContent"></p>
                                    </div>
                                </div>
                                <!-- Services End -->
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <!-- Services Start -->
                                <div class="services-8 d-flex align-items-center">
                                    <!--<span class="icon-block blue-bg"><img src="/images/blocks.png" alt=""></span>-->
                                    <span class="icon-block blue-bg"><i class="icofont-brainstorming"></i></span>
                                    <div class="overflow-text">
                                        <h4 id="txtCreationTitle" class="title"></h4>
                                        <p id="txtCreationContent"></p>
                                    </div>
                                </div>
                                <!-- Services End -->
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <!-- Services Start -->
                                <div class="services-8 d-flex align-items-center">
                                    <!--<span class="icon-block olive-bg"><img src="/images/adventurer.png" alt=""></span>-->
                                    <span class="icon-block olive-bg"><i class="icofont-brainstorming"></i></span>
                                    <div class="overflow-text">
                                        <h4 id="txtMotorTitle" class="title"></h4>
                                        <p id="txtMotorContent"></p>
                                    </div>
                                </div>
                                <!-- Services End -->
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id="doitac" class="tabs-bg pt-130 pb-130 data-overlay" style="position: relative;">
        <!--<div class="fake-title">
            <h6 class="title">choose</h6>
        </div>-->
        <div class="container">
            <!-- Section Title Start Here -->
            <div class="section-title-2 mb-60 white text-center">
                <h2 class="title m-lg-0">Đối tác - Lợi ích </h2>
            </div>
            <!-- Section Title End Here -->
            <div id="BenefitPartners" class="tabs-area">
                <ul class="nav mb-60" id="myTab" role="tablist">
                    <%--<li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Expert Advisor</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Well Results</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Digital Campus</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab2" data-toggle="tab" href="#contact2" role="tab" aria-controls="contact2" aria-selected="false">Scholarship</a>
                    </li>--%>
                </ul>
                <div class="tab-content tabs-caption" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row">
                            <div class="col-lg-5">
                                <figure class="image-thumb-1">
                                    <img id="myImg" src="" alt="">
                                </figure>
                            </div>
                            <div class="col-lg-7 align-self-lg-center">
                                <div id="contentBP" class="text">
                                    <%--<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accustium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illintore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aute.</p>
                                    <h6 class="small-title">Why Needs Expert Advisor</h6>
                                    <p>Learn how to make a What You Get Is What You Hear (WYGIWYH) editor for speech synthesis using Santy.io’s editor for Portable.</p>
                                    <ul class="list-group list-group-icon">
                                        <li class="list-group-item">Used The Web For A Day On Internet Explorer</li>
                                        <li class="list-group-item">A Detailed Comparison Between WordPress And October</li>
                                        <li class="list-group-item">Block Slack’s Contribution To Building A Better Collae</li>
                                    </ul>--%>
                                </div>
                            </div>
                        </div>
                    </div>
<%--                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="row">
                            <div class="col-lg-7 align-self-lg-center">
                                <div class="text">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accustium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illintore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aute.</p>
                                    <h6 class="small-title">Why Needs Expert Advisor</h6>
                                    <p>Learn how to make a What You Get Is What You Hear (WYGIWYH) editor for speech synthesis using Santy.io’s editor for Portable.</p>
                                    <ul class="list-group list-group-icon">
                                        <li class="list-group-item">Used The Web For A Day On Internet Explorer</li>
                                        <li class="list-group-item">A Detailed Comparison Between WordPress And October</li>
                                        <li class="list-group-item">Block Slack’s Contribution To Building A Better Collae</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <figure class="image-thumb-1">
                                    <img src="/images/tabs-img.jpg" alt="">
                                </figure>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        <div class="row">
                            <div class="col-lg-5">
                                <figure class="image-thumb-1">
                                    <img src="/images/tabs-img.jpg" alt="">
                                </figure>
                            </div>
                            <div class="col-lg-7 align-self-lg-center">
                                <div class="text">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accustium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illintore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aute.</p>
                                    <h6 class="small-title">Why Needs Expert Advisor</h6>
                                    <p>Learn how to make a What You Get Is What You Hear (WYGIWYH) editor for speech synthesis using Santy.io’s editor for Portable.</p>
                                    <ul class="list-group list-group-icon">
                                        <li class="list-group-item">Used The Web For A Day On Internet Explorer</li>
                                        <li class="list-group-item">A Detailed Comparison Between WordPress And October</li>
                                        <li class="list-group-item">Block Slack’s Contribution To Building A Better Collae</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="contact2" role="tabpanel" aria-labelledby="contact-tab2">
                        <div class="row">
                            <div class="col-lg-5">
                                <figure class="image-thumb-1">
                                    <img src="/images/tabs-img.jpg" alt="">
                                </figure>
                            </div>
                            <div class="col-lg-7 align-self-lg-center">
                                <div class="text">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accustium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illintore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aute.</p>
                                    <h6 class="small-title">Why Needs Expert Advisor</h6>
                                    <p>Learn how to make a What You Get Is What You Hear (WYGIWYH) editor for speech synthesis using Santy.io’s editor for Portable.</p>
                                    <ul class="list-group list-group-icon">
                                        <li class="list-group-item">Used The Web For A Day On Internet Explorer</li>
                                        <li class="list-group-item">A Detailed Comparison Between WordPress And October</li>
                                        <li class="list-group-item">Block Slack’s Contribution To Building A Better Collae</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>--%>
                </div>
            </div>
        </div>
    </section>

    <section class="pt-130 pb-130">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12">
                    <div class="register-form">
                        <h6 class="title">Đăng ký trở thành đối tác</h6>
                        <div id="registration-form">
                            <div class="input-field form-group">
                                <input class="form-control" value="" id="name" type="text" name="name" maxlength="100" placeholder="Họ và tên" required="" aria-required="true">
                                <span class="icon-box th-bg"><i class="fa fa-user"></i></span>
                            </div>
                            <!--/Input Field End-->
                            <div class="input-field form-group">
                                <input class="form-control" value="" id="email" type="email" name="email" maxlength="100" placeholder="Email" required="" aria-required="true">
                                <span class="icon-box th-bg"><i class="fa fa-envelope"></i></span>
                            </div>
                            <!--/Input Field End-->
                            <div class="input-field form-group">
                                <input class="form-control" value="" id="center" type="text" name="center" maxlength="100" placeholder="Trung tâm" required="" aria-required="true">
                                <span class="icon-box th-bg"><i class="fa fa-book"></i></span>
                            </div>
                            <%--<div class="input-field form-group">
                                <select id="select-courses" name="course" data-msg-required="Vui lòng chọn đối tác" class="form-control" required="" aria-required="true">
                                    <option value="Not Slected">Trung tâm toán tư duy</option>
                                    <option value="English Courses">Trung tâm dạy tư duy & sáng tạo</option>
                                    <option value="Math Courses">Trung tâm ngoại ngữ</option>
                                    <option value="Business Courses">Trung tâm năng khiếu(âm nhạc & múa,...)</option>
                                </select>
                                <span class="icon-box th-bg"><i class="fa fa-book"></i></span>
                            </div>--%>
                            <!--/Input Field End-->
                            <div class="input-field form-group">
                                <input class="form-control" id="phone" value="" type="text" name="phone" maxlength="100" placeholder="Số điện thoại" required="" aria-required="true">
                                <span class="icon-box th-bg"><i class="fa fa-phone"></i></span>
                            </div>
                            <!--/Input Field End-->

                            <div data-aos="fade-up" data-aos-delay="600" class="form-group m-0 text-left">
                            <button id="btnSignUp" type="button" class="btn btn-bigsize">Đăng Ký</button>                            </div>
                            <!--/Input Field End-->
                            <div class="alert alert-success d-none alert-dismissible animated pulse fixed-alert-box" id="registrationSuccess">
                                <button type="button" class="close" data-dismiss="alert">
                                    <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                                </button>
                                Thanks, your message has been sent to us.
                            </div>
                            <div class="alert alert-danger d-none alert-dismissible animated shake fixed-alert-box" id="registrationError">
                                <button type="button" class="close" data-dismiss="alert">
                                    <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                                </button>
                                <strong>Error!</strong> There was an error sending your message.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 ml-lg-minus-3 col-md-12 custom-md-0 hidden-xs hidden-md">
                    <div class="register-img-thumb">
                        <figure class="float-lg-right th-bdr">
                            <img id="myImageSP" src="" alt="">
                        </figure>
                        <div class="osr-countdown blue-bg countdown">
                            <div id="contentSP" class="text-register">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Brand Section Start -->
    <div class="section">
        <div class="container">
            <div class="brand-slide owl-carousel owl-theme">
                <div class="item">
                    <div class="brand-thumb">
                        <img src="/images/img1.png" alt="">
                    </div>
                </div>
                <div class="item">
                    <div class="brand-thumb">
                        <img src="/images/img2.png" alt="">
                    </div>
                </div>
                <div class="item">
                    <div class="brand-thumb">
                        <img src="/images/img3.png" alt="">
                    </div>
                </div>
                <div class="item">
                    <div class="brand-thumb">
                        <img src="/images/img4.png" alt="">
                    </div>
                </div>
                <div class="item">
                    <div class="brand-thumb">
                        <img src="/images/img5.png" alt="">
                    </div>
                </div>
                <div class="item">
                    <div class="brand-thumb">
                        <img src="/images/img6.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Brand Section End -->
    <section class="pt-130 pb-100 event-bg" style="position: relative;">

        <div class="container">
            <!-- Section Title Start Here -->
            <div class="section-title-2 text-center">
                <h2 class="title">Sự kiện</h2>
            </div>
            <!-- Section Title End Here -->
            <div class="filterable-gallery row  masonry clear" style="position: relative; height: 741.562px;">
                <!--Masonry Item Start-->
                <div class="col-md-6 col-lg-6 mix" style="position: absolute; left: 0px; top: 0px;">
                    <!--Event Thumb Strat-->
                    <div class="event-thumb-2">
                        <figure>
                            <img src="/images/event-thumb-7.jpg" alt="">
                        </figure>
                        <div class="text">
                            <div class="max-w">
                                <ul class="blog-meta event-meta">
                                    <li><i class="fa fa-calendar th-cl"></i><a href="#">05 July 2019</a></li>
                                    <li><i class="fa fa-clock-o th-cl"></i>10:00 am - 01:00 pm</li>
                                </ul>
                                <h6 class="title">How To Improve Your Computer Skills</h6>
                            </div>
                            <a class="btn icon-btn" href="/web_module/web_EventDetail.aspx"></a>
                        </div>
                    </div>
                    <!--Event Thumb End-->
                </div>
                <!--Masonry Item End -->
                <!--Masonry Item Start-->
                <div class="col-md-6 col-lg-6 mix" style="position: absolute; left: 585px; top: 0px;">
                    <!--Event Thumb Strat-->
                    <div class="event-thumb-2">
                        <figure>
                            <img src="/images/event-thumb-8.jpg" alt="">
                        </figure>
                        <div class="text">
                            <div class="max-w">
                                <ul class="blog-meta event-meta">
                                    <li><i class="fa fa-calendar th-cl"></i><a href="#">05 July 2019</a></li>
                                    <li><i class="fa fa-clock-o th-cl"></i>10:00 am - 01:00 pm</li>
                                </ul>
                                <h6 class="title">How To Build An Endless Runner Game Virtual</h6>
                            </div>
                            <a class="btn icon-btn" href="/web_module/web_EventDetail.aspx"></a>
                        </div>
                    </div>
                    <!--Event Thumb End-->
                </div>
                <!--Masonry Item End -->
                <!--Masonry Item Start-->
                <div class="col-md-6 col-lg-6 mix" style="position: absolute; left: 585px; top: 370px;">
                    <!--Event Thumb Strat-->
                    <div class="event-thumb-2">
                        <figure>
                            <img src="/images/event-thumb-9.jpg" alt="">
                        </figure>
                        <div class="text">
                            <div class="max-w">
                                <ul class="blog-meta event-meta">
                                    <li><i class="fa fa-calendar th-cl"></i><a href="#">05 July 2019</a></li>
                                    <li><i class="fa fa-clock-o th-cl"></i>10:00 am - 01:00 pm</li>
                                </ul>
                                <h6 class="title">UX/UI Design Meetup Version 01 In 2019</h6>
                            </div>
                            <a class="btn icon-btn" href="/web_module/web_EventDetail.aspx"></a>
                        </div>
                    </div>
                    <!--Event Thumb End-->
                </div>
                <!--Masonry Item End -->
            </div>
            <div class="d-flex mt-50 mb-30 justify-content-center">
                <a class="btn btn-bigsize" href="/web_module/web_Event.aspx">Xem tiếp&nbsp;<i class="fa fa-long-arrow-right"></i></a>
            </div>
        </div>
    </section>

    <!-- Staff Section Start -->
    <section class="blue-bg pt-130 team-bg pb-130">

        <div class="container">
            <!-- Section Title Start Here -->
            <div class="section-title-2 mb-60 white text-left">
                <div class="row d-lg-flex">
                    <div class="col-lg-9 col-md-9">
                        <h2 class="title m-0">Chuyên gia</h2>
                    </div>
                    <div class="col-lg-3 col-md-3 text-lg-right align-self-lg-center align-self-md-center">
                        <a href="/web_module/web_Teacher.aspx" class="btn btn-bigsize white-bg">Xem thêm&nbsp;<i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- Section Title End Here -->
            <div class="row">
                <div class="nav-style-1 staff-slide owl-carousel owl-theme">
                    <div class="item">
                        <!-- Team Thumb Start Here -->
                        <a href="/web_module/web_TeacherDetail.aspx" class="team-thumb text-center">
                            <div class="blog-img">
                                <img src="/images/team-thumb-1.jpg" alt="blog" />
                            </div>
                            <div class="text">
                                <h4 class="title">Michel DZ Thomas</h4>
                                <p class="designation th-cl">Math Teacher</p>
                            </div>
                        </a>
                        <!-- Team Thumb End Here -->
                    </div>
                    <div class="item">
                        <!-- Team Thumb Start Here -->
                        <a href="/web_module/web_TeacherDetail.aspx" class="team-thumb text-center">
                            <div class="blog-img">
                                <img src="/images/team-thumb-2.jpg" alt="blog" />
                            </div>
                            <div class="text">
                                <h4 class="title">Glassy PHD Model</h4>
                                <p class="designation th-cl">Wed designer</p>

                            </div>
                        </a>
                        <!-- Team Thumb End Here -->
                    </div>
                    <div class="item">
                        <!-- Team Thumb Start Here -->
                        <a href="/web_module/web_TeacherDetail.aspx" class="team-thumb text-center">
                            <div class="blog-img">
                                <img src="/images/team-thumb-3.jpg" alt="blog" />
                            </div>
                            <div class="text">
                                <h4 class="title">David Smile Hiksa</h4>
                                <p class="designation th-cl">sr Engineer</p>

                            </div>
                        </a>
                        <!-- Team Thumb End Here -->
                    </div>
                    <div class="item">
                        <!-- Team Thumb Start Here -->
                        <a href="/web_module/web_TeacherDetail.aspx" class="team-thumb text-center">
                            <figure class="blog-img">
                                <img src="/images/team-thumb-4.jpg" alt="blog" />
                            </figure>
                            <div class="text">
                                <h4 class="title">Sonakila Blue Bard</h4>
                                <p class="designation th-cl">Math Teacher</p>
                            </div>
                        </a>
                        <!-- Team Thumb End Here -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Staff Section End -->
    <section id="camnang" class="pt-130 pb-100">
        <div class="container">
            <!-- Section Title Start Here -->
            <div class="section-title-2 text-center">
                <h2 class="title">Cẩm nang</h2>
            </div>
            <!-- Section Title End Here -->
            <div class="row">
                <div class="nav-style-1 staff-slide owl-carousel owl-theme">
                    <div class="item">
                        <!-- Blog Thumb Start Here -->
                        <div class="blog-thumb">
                            <figure class="blog-img">
                                <div class="khungbao" style="background-image: url('/images/blog-thumb-1.jpg');">
                                </div>
                            </figure>
                            <div class="text">
                                <ul class="blog-meta">
                                    <li><i class="fa fa-user th-cl"></i><a href="#">Jondy Ross</a></li>
                                    <li><i class="fa fa-calendar th-cl"></i><a href="#">05 July 2019</a></li>
                                </ul>
                                <h4 class="title"><a href="/web_module/web_NewsDetail.aspx">How Screen Reader User Accesses Web Video</a></h4>
                                <p>But must explain to you how this mistaken idea of denouncing pleasure and praising pain was born and I will give you </p>
                                <a class="readmore-btn" href="/web_module/web_NewsDetail.aspx">Đọc tiếp <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                        <!-- Blog Thumb End Here -->
                    </div>
                    <div class="item">
                        <!-- Blog Thumb Start Here -->
                        <div class="blog-thumb">
                            <figure class="blog-img">
                                <div class="khungbao" style="background-image: url('/images/blog-thumb-2.jpg'); height: 230px">
                                </div>
                            </figure>
                            <div class="text">
                                <ul class="blog-meta">
                                    <li><i class="fa fa-user th-cl"></i><a href="#">Jondy Ross</a></li>
                                    <li><i class="fa fa-calendar th-cl"></i><a href="#">05 July 2019</a></li>
                                </ul>
                                <h4 class="title"><a href="/web_module/web_NewsDetail.aspx">Web Development Update React Hook Use Reader</a></h4>
                                <p>But must explain to you how this mistaken idea of denouncing pleasure and praising pain was born and I will give you </p>
                                <a class="readmore-btn" href="/web_module/web_NewsDetail.aspx">Đọc tiếp <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                        <!-- Blog Thumb End Here -->
                    </div>
                    <div class="item">
                        <!-- Blog Thumb Start Here -->
                        <div class="blog-thumb">
                            <figure class="blog-img">
                                <div class="khungbao" style="background-image: url('/images/blog-thumb-3.jpg'); height: 230px">
                                </div>
                            </figure>
                            <div class="text">
                                <ul class="blog-meta">
                                    <li><i class="fa fa-user th-cl"></i><a href="#">Jondy Ross</a></li>
                                    <li><i class="fa fa-calendar th-cl"></i><a href="#">05 July 2019</a></li>
                                </ul>
                                <h4 class="title"><a href="/web_module/web_NewsDetail.aspx">How Screen Reader User Accesses Web Video</a></h4>
                                <p>But must explain to you how this mistaken idea of denouncing pleasure and praising pain was born and I will give you </p>
                                <a class="readmore-btn" href="/web_module/web_NewsDetail.aspx">Đọc tiếp <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                        <!-- Blog Thumb End Here -->
                    </div>
                    <div class="item">
                        <!-- Blog Thumb Start Here -->
                        <div class="blog-thumb">
                            <figure class="blog-img">
                                <div class="khungbao" style="background-image: url('/images/blog-thumb-2.jpg'); height: 230px">
                                </div>
                            </figure>
                            <div class="text">
                                <ul class="blog-meta">
                                    <li><i class="fa fa-user th-cl"></i><a href="#">Jondy Ross</a></li>
                                    <li><i class="fa fa-calendar th-cl"></i><a href="#">05 July 2019</a></li>
                                </ul>
                                <h4 class="title"><a href="/web_module/web_NewsDetail.aspx">Web Development Update React Hook Use Reader</a></h4>
                                <p>But must explain to you how this mistaken idea of denouncing pleasure and praising pain was born and I will give you </p>
                                <a class="readmore-btn" href="/web_module/web_NewsDetail.aspx">Đọc tiếp <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                        <!-- Blog Thumb End Here -->
                    </div>
                    <div class="item">
                        <!-- Blog Thumb Start Here -->
                        <div class="blog-thumb">
                            <figure class="blog-img">
                                <div class="khungbao" style="background-image: url('/images/blog-thumb-1.jpg'); height: 230px">
                                </div>
                            </figure>
                            <div class="text">
                                <ul class="blog-meta">
                                    <li><i class="fa fa-user th-cl"></i><a href="#">Jondy Ross</a></li>
                                    <li><i class="fa fa-calendar th-cl"></i><a href="#">05 July 2019</a></li>
                                </ul>
                                <h4 class="title"><a href="/web_module/web_NewsDetail.aspx">How Screen Reader User Accesses Web Video</a></h4>
                                <p>But must explain to you how this mistaken idea of denouncing pleasure and praising pain was born and I will give you </p>
                                <a class="readmore-btn" href="/web_module/web_NewsDetail.aspx">Đọc tiếp <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                        <!-- Blog Thumb End Here -->
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="hibelowbottom" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="hifooter" runat="Server">
    <script type="text/javascript">
        $(document).ready(() => {
            $.ajax({
                type: "GET",
                url: "https://localhost:44334/api/TbSlide",
                datatype: "JSON",
                success: (data) => {
                    //console.log(data);
                    $("#rpslide").empty();
                    var htmlimport = '';
                    $.each(data, function (i, item) {

                        htmlimport += "<div class='item bg-item'><div class='container'><div class='justify-content-center'><div class='content-block wow fadeInLeft text-left' data-wow-delay='1.5s'>";
                        htmlimport += "<div class='small-title th-bg badge'>" + item.slideSubtitle + "</div>";
                        htmlimport += "<h2>" + item.slideTitle + "</h2>";
                        htmlimport += "<p>" + item.slideDescription + "</p>";
                        htmlimport += "<a class='btn btn-slide' href='#'>Liên hệ ngay</a>";
                        htmlimport += "</div></div></div></div>";
                        //htmlimport += "<div class='item bg-item'><div class='container'><div class=' justify-content-center'>";
                        //htmlimport += "<div id='addslide' class='content-block wow fadeInLeft text-left' style='visibility: visible;' data-wow-delay='1.5s'>";
                        //htmlimport += "<div class='small-title th-bg badge'>education makes bright your future</div>";
                        //htmlimport += "<h2>BEEKIDS <br /> Câu Chuyện Đồ Gỗ </h2>";
                        //htmlimport += "<p> Đến với Beekids, các bé sẽ phát triển trí não toàn diện trong môi trường giáo dục chất lượng cao. </p>";
                        //htmlimport += "<a class='btn btn-slide' href='#'>Liên hệ ngay </a>";
                        //htmlimport += "</div> </div> </div> </div>";

                    })
                    $("#rpslide").append(htmlimport);
                    $('.slide-block1').owlCarousel({
                        loop: true,
                        margin: 0,
                        dots: false,
                        autoplay: false,
                        items: 1,
                        nav: true,
                        navText: ["prev", "next"],
                        pagination: false,
                        navigation: true,
                        center: false,
                        autoplayTimeout: 4000,
                        responsive: {
                            0: {
                                items: 1
                            },
                            600: {
                                items: 1
                            },
                            1000: {
                                items: 1
                            }
                        }
                    })
                }

            })
        })
    </script>

    <script type="text/javascript">
        $(document).ready(() => {
            fetch("https://localhost:44334/api/TbSection1").then(
                res => {
                    res.json().then(
                        data => {
                            $.each(data, (i, item) => {
                                document.getElementById('txtTitleSection1').innerHTML = item.section1Title;
                                document.getElementById('txtDescriptionSection1').innerHTML = item.section1Description;
                                document.getElementById('txtProductTitle').innerHTML = item.section1ProductTitle;
                                document.getElementById('txtProductContent').innerHTML = item.section1ProductContent;
                                document.getElementById('txtPriceTitle').innerHTML = item.section1PriceTitle;
                                document.getElementById('txtPriceContent').innerHTML = item.section1PriceContent;
                                document.getElementById('txtDeliveryTitle').innerHTML = item.section1DeliveryTitle;
                                document.getElementById('txtDeliveryContent').innerHTML = item.section1DeliveryContent;
                                document.getElementById('txtQualityTitle').innerHTML = item.section1QualityTitle;
                                document.getElementById('txtQualityContent').innerHTML = item.section1QualityContent;

                            })

                        }
                    )
                })

            fetch("https://localhost:44334/api/TbSection2").then(
                res => {
                    res.json().then(
                        data => {
                            $.each(data, (i, item) => {
                                document.getElementById('txtTitleSection2').innerHTML = item.section2Title;
                                document.getElementById('txtDescriptionSection2').innerHTML = item.section2Description;
                                document.getElementById('txtThinkingTitle').innerHTML = item.section2ThinkingTitle;
                                document.getElementById('txtThinkingContent').innerHTML = item.section2ThinkingContent;
                                document.getElementById('txtBrainTitle').innerHTML = item.section2BrainTitle;
                                document.getElementById('txtBrainContent').innerHTML = item.section2BrainContent;
                                document.getElementById('txtCreationTitle').innerHTML = item.section2CreationTitle;
                                document.getElementById('txtCreationContent').innerHTML = item.section2CreationContent;
                                document.getElementById('txtMotorTitle').innerHTML = item.section2MotorTitle;
                                document.getElementById('txtMotorContent').innerHTML = item.section2MotorContent;
                            })
                        })
                })

            fetch("https://localhost:44334/api/TbBenefitPartners").then(
                res => {
                    res.json().then(
                        data => {
                            $("#myTab").empty();
                            var temp = '';
                            $.each(data, (i, item) => {
                                temp += "<li class='nav-item'>",
                                temp += "<a class='nav-link' onclick='loadContent(" + item.bpId + ")' id='home-tab' data-toggle='tab' href='#home' role='tab' aria-controls='home' aria-selected='true'>" + item.bpTitle + "</a>"
                                temp += "</li>"
                            })
                            $("#myTab").append(temp);
                            $('.nav-item li').click(function () {
                                $(this).addClass('active').siblings().removeClass('active');
                            });

                        })
                })

            fetch("https://localhost:44334/api/TbSignUpPartnerr").then(
                res => {
                    res.json().then(
                        data => {
                            $.each(data, (i, item) => {
                                document.getElementById('contentSP').innerHTML = item.spContent;
                                document.getElementById('myImageSP').src = item.spImage;
                            })
                        })
                })
        })

        function loadContent(_id) {
            var url = "https://localhost:44334/api/TbBenefitPartners/" + _id;
            jQuery.ajax({
                type: "GET",
                url: url,
                datatype: "JSON",
                success: (data) => {
                    document.getElementById('contentBP').innerHTML = data.bpDescription;
                    document.getElementById("myImg").src = data.bpImg;
                }
            });
        }

    </script>

    <script type="text/javascript" >
        jQuery(document).ready(($) => {
            $('#btnSignUp').click(function () {
                var name = $("[id*=name]").val();
                var email = $("[id*=email]").val();
                var center = $("[id*=center]").val();
                var phone = $("[id*=phone]").val();
                $.ajax({
                    type: "POST",
                    url: "https://localhost:44334/api/tbWebSignUps",
                    contentType: "application/json; charset=utf-8",
                    data: '{"webSignupId":0,"webSignupName":"' + name + '","webSignupEmail":"' + email + '","webSignupPhonenb":"' + phone + '","webSignupCenterName":"' + center + '"}',
                    dataType: "json",
                    success: function (data) {
                        console.log('{"webSignupId":0,"webSignupName":"' + name + '","webSignupEmail":"' + email + '","webSignupPhonenb":"' + phone + '","webSignupCenterName":"' + center + '"}')
                    }
                });
            });
        })
    </script>
</asp:Content>


